#include <stdlib.h>
#include <stdio.h>

int main(int argc, char** argv){
        if(argc > 2){
                unsigned long long int lower = strtoull(argv[1], NULL, 10);
                unsigned long long int upper = strtoull(argv[2], NULL, 10);
                printf("%d %d\n", lower, upper);
                if(lower > upper){
                        puts("INCORRECT: limite basse doit être inférieure à la limite haute\n");
                        return -1;
                }
                for(unsigned long long int i = lower; i < upper; i++){
                        for (unsigned long long int j = 1; j <= i; j++){
                                if(j == i)
                                        printf("prime : %d\n",i);
                                if(i % j == 0 && j != 1)
                                        break;
                        }
                }
        }
        return 0;
}
